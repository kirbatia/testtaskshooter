using System;
using UnityEngine;

namespace UI
{
    public class MainUiController : MonoBehaviour
    {
        [SerializeField] private MainWindowController _mainWindowController;
        [SerializeField] private WeaponWindowController _weaponWindowController;
        [SerializeField] private GameOverWindowController _gameOverWindowController;

        private PlayerDataManager _playerDataManager;
        private WeaponManager _weaponManager;

        public event Action RestartClicked;

        public void Init(PlayerDataManager playerDataManager, WeaponManager weaponManager)
        {
            _playerDataManager = playerDataManager;
            _weaponManager = weaponManager;

            UpdateActiveWindows(true);
            _mainWindowController.Init(_playerDataManager);
            _weaponWindowController.Init(_weaponManager);

            _gameOverWindowController.RestartClicked += OnRestartClicked;
            _playerDataManager.PlayerKilled += OnPlayerKilled;
        }

        private void OnRestartClicked()
        {
            RestartClicked?.Invoke();
        }

        private void OnPlayerKilled()
        {
            Time.timeScale = 0;
            UpdateActiveWindows(false);
        }

        private void UpdateActiveWindows(bool isActive)
        {
            _gameOverWindowController.gameObject.SetActive(!isActive);
            _weaponWindowController.gameObject.SetActive(isActive);
            _mainWindowController.gameObject.SetActive(isActive);
        }

        private void OnDisable()
        {
            _gameOverWindowController.RestartClicked -= OnRestartClicked;
            _playerDataManager.PlayerKilled -= OnPlayerKilled;
        }
    }
}