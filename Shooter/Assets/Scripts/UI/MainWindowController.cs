using TMPro;
using UnityEngine;

namespace UI
{
   public class MainWindowController : MonoBehaviour
   {
      [SerializeField] private TextMeshProUGUI _score;
      [SerializeField] private TextMeshProUGUI _hp;
      [SerializeField] private TextMeshProUGUI _controlInfo;
 
      private PlayerDataManager _playerDataManager;
      private WeaponManager _weaponManager;

      public void Init(PlayerDataManager playerData)
      {
         _playerDataManager = playerData;
         var data = _playerDataManager.PlayerData;
         UpdateScore(data.Score);
         OnHealthChanged(data.Health);
         SetTextControlsInfo();
         _playerDataManager.HealthChanged += OnHealthChanged;
         _playerDataManager.ScoreChanged += OnScoreChanged;
      }

      private void OnScoreChanged(int score)
      {
         UpdateScore(score);
         _playerDataManager.UpdateFirstPlayerInfo(false);
         _controlInfo.enabled = false;
      }

      private void UpdateScore(int score)
      {
         _score.text = $"Score : {score}";
      }

      private void OnHealthChanged(int hp)
      {
         _hp.text = $"HP : {hp}";
      }

      private void OnDisable()
      {
         _playerDataManager.HealthChanged -= OnHealthChanged;
         _playerDataManager.ScoreChanged -= OnScoreChanged;
      }

      private void SetTextControlsInfo()
      {
         if (!_playerDataManager.IsFirstPlayer)
         {
            return;
         }
         _controlInfo.enabled = true;
         _controlInfo.text ="Controls:\n<- Turn left\n-> Turn right\nKey.Space Fire\nKey.A Change weapon\nKey.D Recharge";
      }
   }
}