using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class WeaponIndicatorItem : MonoBehaviour
    {
        [SerializeField] private Image _weapon;
        [SerializeField] private TextMeshProUGUI _count;
        [SerializeField] private Image _activeWeapon;
        [SerializeField] private WeaponType _weaponType;
        [SerializeField] private TextMeshProUGUI _recharge;
        private WeaponController _weaponController;
        private int _cachedBulletCount;

        private bool _isActive;
        public WeaponType WeaponType => _weaponType;

        public void Init(WeaponController weaponController)
        {
            _weaponController = weaponController;
            UpdateItem();
            _cachedBulletCount = _weaponController.CachedBulletCount;
            OnCountChanged();
            _weaponController.BulletCountChanged += OnCountChanged;
            _weaponController.RechargeChanged += OnRechargeChanged;
            _weaponController.WeaponChanged += OnWeaponChanged;
            _weaponController.CoolDownSet += OnCoolDownSet;
        }

        private void OnCoolDownSet(bool isActive)
        {
            _weapon.color = isActive? Color.red : Color.white;
        }

        private void OnWeaponChanged()
        {
            UpdateItem();
        }

        private void OnRechargeChanged(float time)
        {
            if (time >= _weaponController.RechargeTimeTotal || time <= 0)
            {
                _recharge.enabled = false;
                _weapon.color = Color.white;
            }
            else
            {
                _recharge.enabled = true;
                _weapon.color = Color.red;
            }
            _recharge.text = $"Recharging... {time} / {_weaponController.RechargeTimeTotal}";
        }

        private void OnCountChanged()
        {
            _count.text = $"{_weaponController.BulletsLeft} / {_cachedBulletCount}";
        }

        private void UpdateItem()
        {
            _isActive = _weaponController.IsActive;
            _activeWeapon.color = _isActive ? Color.cyan : Color.gray;
        }

        private void OnDisable()
        {
            _weaponController.BulletCountChanged -= OnCountChanged;
            _weaponController.RechargeChanged -= OnRechargeChanged;
            _weaponController.WeaponChanged -= OnWeaponChanged;
            _weaponController.CoolDownSet -= OnCoolDownSet;
        }
    }
}