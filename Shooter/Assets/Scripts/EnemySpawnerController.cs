using System;
using UnityEngine;

public class EnemySpawnerController : MonoBehaviour
{
    private const float SpawnSpeed = 0.3f;
    
    [SerializeField] private EnemyController _enemyController;
    private EnemySpawner _enemySpawner;
    private Coroutine _spawn;

    public event Action PlayerHit;

    public void Init(WeaponManager weaponManager)
    {
        _enemySpawner = new EnemySpawner(_enemyController, weaponManager);
        _enemySpawner.Init();
        _enemySpawner.PlayerHit += OnPlayerHit;
        _spawn = StartCoroutine(_enemySpawner.Spawn(SpawnSpeed));
    }

    private void OnPlayerHit()
    {
        PlayerHit?.Invoke();
    }

    private void OnDestroy()
    {
        StopCoroutine(_spawn);
        _enemySpawner.Destroy();
    }
}