using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager: MonoBehaviour
{
   private const int BulletPoolSize = 3;
   
  [SerializeField] private List<WeaponController> _weaponLists;
  [SerializeField] private GameObject _bullet;

   private InputController _inputController;
   private int _currentWeaponIndex;
   private WeaponController _activeWeapon;
   private ObjectPool _pool;
   private List<WeaponController> _weaponControllers = new List<WeaponController>();
   private List<BulletController> _bullets = new List<BulletController>();
   private bool _canFire = true;
   private Coroutine _recharge;

   public List<WeaponController> WeaponControllers => _weaponControllers;
   public WeaponType ActiveWeaponType => _activeWeapon.WeaponType;
   public event Action<Collider> BulletTriggered;

   public void Init(InputController inputController)
   {
      _currentWeaponIndex = 0;
      SwitchWeapon();
      _inputController = inputController;
      _inputController.KeyPressed += OnKeyPressed;
      _pool = new ObjectPool(_bullet, BulletPoolSize);
   }

   private void OnKeyPressed(KeyCode key)
   {
      switch (key)
      {
         case KeyCode.A:
         {
            StopRecharge();
            _canFire = true;
            SwitchWeapon();
            break;
         }
         case KeyCode.D:
         {
            StopRecharge();
            _recharge = StartCoroutine(Recharge());
            break;
         }
         case KeyCode.Space:
         {
            Fire();
            break;
         }
      }
   }

   private void StopRecharge()
   {
      if (_recharge != null)
      {
         StopCoroutine(_recharge);
      }
   }

   private IEnumerator Recharge()
   {
      while (_activeWeapon.RechargeTimeLeft < _activeWeapon.RechargeTimeTotal)
      {
         _canFire = false;
         _activeWeapon.RechargeTimeLeft++;
         yield return new WaitForSeconds(1f);
      }
      _activeWeapon.RechargeTimeLeft = 0;
      _activeWeapon.BulletsLeft = _activeWeapon.CachedBulletCount;
      _canFire = true;
   }
   
   private void SwitchWeapon()
   {
      _currentWeaponIndex = (_currentWeaponIndex + 1) % _weaponLists.Count;
      for (int i = 0; i < _weaponLists.Count; i++)
      {
         var item = _weaponLists[i];
         item.gameObject.SetActive(i == _currentWeaponIndex);
         item.IsActive = i == _currentWeaponIndex;
         item.Init();
         _weaponControllers.Add(item);
      }
      _activeWeapon = _weaponLists[_currentWeaponIndex];
   }

   private void Fire()
   {
      var activeType = _activeWeapon.WeaponType;

      switch (activeType)
      {
         case WeaponType.Bfg:
         {
            ShootWithGun();
            break;
         }
         case WeaponType.Shotgun:
         case WeaponType.DBShotgun:
         {
            ShootWithRay(_activeWeapon);
            break;
         }
      }
   }

   private void OnBulletTriggered(Collider obj, BulletController bulletController = null)
   {
      if (bulletController != null)
      {
         ReturnBulletToPool(bulletController.gameObject);;
      }
      
      BulletTriggered?.Invoke(obj);
   }

   private void ShootWithGun()
   {
      if (_activeWeapon.BulletsLeft <= 0 || !_canFire)
      {
         return;
      }
      
      var bullet = _pool.GetObject<BulletController>();
      var bulletTransform = bullet.transform;
      bulletTransform.position = _activeWeapon.FireSpawnPosition.position;
      bulletTransform.rotation = _activeWeapon.FireSpawnPosition.rotation;
      bullet.Shoot();
      bullet.Triggered += OnBulletTriggered;
      _bullets.Add(bullet);
      _activeWeapon.BulletsLeft--;
      StartCoroutine(ReturnAfterDelay(bullet.gameObject, 1.5f));
      StartCoroutine(CoolDown(_activeWeapon));
   }
   
   private void ShootWithRay(WeaponController weapon)
   {
      if (weapon.BulletsLeft <= 0 || !_canFire)
      {
         return;
      }
      
      Ray ray = new Ray(weapon.FireSpawnPosition.position, transform.forward);
      var laser = weapon.LaserRenderer;
      laser.enabled = true;
      var laserPosition = weapon.FireSpawnPosition;
      if (Physics.Raycast(ray, out var hitInfo))
      {
         var enemy = hitInfo.collider.gameObject;
         laser.SetPosition(0, laserPosition.position);
         laser.SetPosition(1, hitInfo.point);
         OnBulletTriggered(hitInfo.collider);
      }
      else
      {
         laser.SetPosition(0, laserPosition.position);
         laser.SetPosition(1, laserPosition.position + laserPosition.forward * 100);
      }
      StartCoroutine(DisableLaser(laser));
      
      if (weapon.WeaponType == WeaponType.DBShotgun)
      {
         weapon.BulletsLeft-= 2;
      }
      else
      {
         weapon.BulletsLeft--;
      }
      StartCoroutine(CoolDown(_activeWeapon));
   }

   private IEnumerator DisableLaser(LineRenderer line)
   {
      yield return new WaitForSeconds(0.1f);
      line.enabled = false;
   }
   
   private IEnumerator ReturnAfterDelay(GameObject obj, float time)
   {
      yield return new WaitForSeconds(time);
      ReturnBulletToPool(obj);
   }

   private void ReturnBulletToPool(GameObject obj)
   {
      var bullet = obj.GetComponent<BulletController>();
      bullet.Triggered -= OnBulletTriggered;
      _bullets.Remove(bullet);
      _pool.ReturnObject(obj);
   }

   private IEnumerator CoolDown(WeaponController weaponController)
   {
      _canFire = false;
      weaponController.IsCoolDownActive = true;
      yield return new WaitForSeconds(weaponController.CoolDown);
      weaponController.IsCoolDownActive = false;
      _canFire = true;
   }
   
   private void OnDisable()
   {
      _inputController.KeyPressed -= OnKeyPressed;
      foreach (var bullet in _bullets)
      {
         bullet.Triggered -= OnBulletTriggered;
      }
      _bullets.Clear();
      StopAllCoroutines();
   }
}