using UnityEngine;

[CreateAssetMenu]
public class PlayerData: ScriptableObject
{
    public int Health;
    public int Score;
    public bool IsFirstPlayer;
}